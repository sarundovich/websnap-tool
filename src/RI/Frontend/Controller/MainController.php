<?php
namespace RI\Frontend\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class MainController {
	public function indexAction(Request $request, Application $app) {
		return $app['twig']->render('front/main/index.twig');
	}

    public function benefitsAction(Request $request, Application $app) {
        return $app['twig']->render('front/main/benefits.twig');
    }

	public function snapshotAction(Request $request, Application $app) {
        $resp = array(
            'success' => false,
        );

        $format = 'png';
        $quality = '100';
		$url = $request->get('url');
        $cmd = $app['phantomjs'].' '.$app['dir.src'].'/phantom.js '.$url.' '.$app['dir.tmp'].' '.$format.' '.$quality;

        $result = shell_exec($cmd);
        preg_match('/(<.*?[^>]>)(.*)/', $result, $matches);
        $json = preg_replace('/(<.*?[^>]>)/', '', $matches[0]);

        if ($app['isJson']($json)) {
            $result = json_decode($json);

            if ($result->success) {
                $resp['success'] = true;
                $resp['filename'] = $result->filename;
            }
        }

        return $app->json($resp);
	}
}