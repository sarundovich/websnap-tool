<?php
namespace RI\Frontend\Controller\Provider;

use Silex\Application;
use Silex\ControllerProviderInterface;

class MainControllerProvider implements ControllerProviderInterface {

	public function connect(Application $app) {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];
        
        /* begin provider route mappings */
        $controllers->get('/', 'RI\Frontend\Controller\MainController::indexAction');
        $controllers->post('/snapshot', 'RI\Frontend\Controller\MainController::snapshotAction');
        /* end provider route mappings */

        return $controllers;
    }
}