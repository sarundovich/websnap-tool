var system = require('system');
var page = require('webpage').create();
var url = system.args[1];
var imgPath = system.args[2];
var imgFormat = system.args[3];
var imgQuality = system.args[4];

var callback = function(result) {
    //Give a call for PHP invoker
    system.stdout.write('<response>'+JSON.stringify(result)+'</response>');
    //Quit PhantomJs
    phantom.exit(1);
};

var makeName = function(num) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < num; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};
page.onConsoleMessage = function (msg) { return false; };
page.viewportSize = { width: 1366, height: 768 };
page.open(url, function(status) {
    var resp = {
        'success': false
    }

    if (status == 'success') {
        var fileName = makeName(7) + "." + imgFormat;
        var path = imgPath + '/' + fileName
        page.render(path, {format: imgFormat, quality: imgQuality});
        resp.success = true;
        resp.filename = fileName;
    }

    callback(resp);
});
