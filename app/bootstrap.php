<?php
require_once __DIR__.'/../vendor/autoload.php';
define('VIEWS_DIR', __DIR__.'/../views');
define('CACHE_DIR', __DIR__.'/../cache');
define('CONFIG_DIR', __DIR__.'/../config');
define('TMP_DIR', __DIR__.'/../web/tmp');
define('PHANTOM', __DIR__.'/../bin/phantomjs');
define('SRC_DIR', __DIR__.'/../src');