<?php
require_once __DIR__.'/bootstrap.php';

use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => VIEWS_DIR,
    'twig.options' => array(
    	'cache' => CACHE_DIR.'/views',
        'auto_reload' => true,
	),
    'twig.autoescape' => true,
));

$app['phantomjs'] = PHANTOM;
$app['dir.tmp'] = TMP_DIR;
$app['dir.tmp_web'] = '../tmp';
$app['dir.src'] = SRC_DIR;

$app['isJson'] = $app->protect(function($str) {
    json_decode($str);
    return (json_last_error() == JSON_ERROR_NONE);
});

/* Mount controller providers */
$app->mount('/', new RI\Frontend\Controller\Provider\MainControllerProvider());

/* Handle app errors */
$app->error(function (\Exception $e, $code) {
    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }

    return new Response($message);
});

// Return configured app
// to the front controller
return $app;