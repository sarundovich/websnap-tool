$(document).ready(function(){
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        return $(this).ekkoLightbox();
    });

    initPopover();

    $("#trig-attention").click(function() {
        $("#trig-attention").popover('toggle');
        return false;
    });

    $("#take-screenshot-btn").click(function(){
        var url = $("#webpageurl").val();

        if (url == null || url == false || url == '') {
            var msg = "URL can't be empty or wrong format used.";
            showAlert(getAlert(msg));
        } else {
            var urlRegExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;

            if (!urlRegExp.test(url)){
                $("#alert-msg").append(alert);
            }else{
                $.ajax({
                    type: "POST",
                    url: "/snapshot",
                    data: {
                        url: url
                    },
                    beforeSend: function() {
                        disableSnapInputs();
                    },
                    success: function(data) {
                        enableSnapInputs();

                        if (data['success']) {
                            showLightBox(url, data['filename']);
                        } else {
                            var msg = "Something wrong. Sorry!";
                            showAlert(getAlert(msg));
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {}
                });
            }
        }

        return false;
    });
});

function initPopover() {
    var benefitsDescription = "<ul>" +
        "<li><span class='blue'>No Watermarks:</span> get your screenshots without watermarks</li>" +
        "<li><span class='blue'>Image Types:</span> you can choose your image type - jpeg, png or even PDF!</li>" +
        "<li><span class='blue'>Image Quality:</span> you can choose image quality you need</li>" +
        "<li><span class='blue'>Resolutions:</span> various screenshot resolutions available</li>" +
        "<li><span class='blue'>Image Gallery:</span> storing your images in handy galleries</li>" +
        "</ul>" +
        "<a href='/benefits'>Learn more</a>";
    var popoverOpt = {
        trigger: 'focus',
        html: true,
        content: benefitsDescription,
        placement: 'auto',
        title: '<i class="fa fa-info-circle"></i> Get your benefits now!'
    };

    $("#trig-attention").popover(popoverOpt);
}

function showAlert(alert) {
    $("#alert-msg").empty();
    $("#alert-msg").append(alert);
}

function getAlert(str) {
    return "<div class='alert alert-danger fade in' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>×</span><span class='sr-only'>Close</span></button><strong>We have got a problem!</strong>  "+str+"</div>";
}

function showLightBox(url, file) {
    $('#img-container').empty();
    $("#img-container").append('<a id="snap-shot" href="tmp/'+file+'" data-toggle="lightbox" data-title="URL: '+url+'" data-footer=""></a>');
    $('#snap-shot').ekkoLightbox();
}

function disableSnapInputs() {
    $("#take-screenshot-btn").empty().html('<i class="fa fa-cog fa-spin"></i> Working...');
    $("#take-screenshot-btn").attr('disabled', true);
    $("#webpageurl").attr('disabled', true);
}

function enableSnapInputs() {
    $("#take-screenshot-btn").empty().html('<i class="fa fa-camera-retro fa-lg"></i> Snap It');
    $("#take-screenshot-btn").attr('disabled', false);
    $("#webpageurl").attr('disabled', false);
}